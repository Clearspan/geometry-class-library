﻿namespace GeometryClassLibrary
{
    public partial class Direction
    {
        public int? DatabaseId { get; set; }

        public int? Phi_DatabaseId { get; set; }

        public int? Theta_DatabaseId { get; set; }
    }
}
