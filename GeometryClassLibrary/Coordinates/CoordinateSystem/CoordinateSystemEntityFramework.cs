﻿namespace GeometryClassLibrary
{
    public partial class CoordinateSystem
    {
        public int? DatabaseId { get; set; }

        public int? ShiftFromThisToWorld_DatabaseId { get; set; }
    }
}
