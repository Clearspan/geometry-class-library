﻿namespace GeometryClassLibrary
{
    public class Enums
    {
        public enum Axis { X, Y, Z };
        public enum AxisPlanes { XYPlane, XZPlane, YZPlane };
    }
}
