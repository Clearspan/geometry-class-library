﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnitClassLibrary;

namespace GeometryClassLibrary
{
    public partial class Polygon
    {
        public static Polygon EquilateralTriangle(Distance sideLength)
        {
            return RegularPolygon(3, sideLength);
        }

        /// <summary>
        /// Creates a parrelologram. 
        /// shifts both vectors so their basepoints are the passed basepoint, and creates the parrelogram spanned by those sides.
        /// </summary>
        public static Polygon Parallelogram(Vector vector1, Vector vector2, Point basePoint = null)
        {
            if (basePoint == null)
            {
                basePoint = vector1.BasePoint;
            }
            LineSegment segment1 = new LineSegment(basePoint, vector1);
            LineSegment segment2 = new LineSegment(basePoint, vector2);
            LineSegment segment3 = new LineSegment(segment2.EndPoint, vector1);
            LineSegment segment4 = new LineSegment(segment1.EndPoint, vector2);

            return new Polygon(new List<LineSegment>() { segment1, segment2, segment3, segment4 });
        }

        public static Polygon Pentagon(Distance sideLength)
        {
            return RegularPolygon(5, sideLength);
        }

        public static Polygon Rectangle(Distance xLength, Distance yLength, Point basePoint = null)
        {
            if (basePoint == null)
            {
                basePoint = Point.Origin;
            }
            var vector1 = new Vector(basePoint, Direction.Right, xLength);
            var vector2 = new Vector(basePoint, Direction.Up, yLength);
            return Parallelogram(vector1, vector2);
        }

        /// <summary>
        /// Creates a regular polygon centered at the origin in the XY-plane.
        /// </summary>
        public static Polygon RegularPolygon(int numberOfSides, Distance sideLength, Angle startingAngle = null, Point centerPoint = null)
        {
            if (numberOfSides < 3)
            {
                throw new ArgumentException("A polygon must have more than 2 sides.");
            }

            AngularDistance step = Angle.Degree * 360.0 / numberOfSides;
            AngularDistance otherAngle = (Angle.Degree * 180 - step) / 2;

            //Law of Sines
            Distance length = sideLength * Math.Sin(otherAngle.Radians) / Math.Sin((step.Radians));

            Point firstPoint;
            if (startingAngle == null)
            {   // We want the polygon to be centered at the origin,
                // and lie "flat" from the viewers perspective
                if (numberOfSides % 4 == 0)
                {
                    firstPoint = new Point(length, Distance.Zero);
                    firstPoint = firstPoint.Rotate2D(step / 2);
                }
                else if (numberOfSides % 2 == 0)
                {
                    firstPoint = new Point(length, Distance.Zero);
                }
                else
                {
                    firstPoint = new Point(Distance.Zero, length);
                }
            }
            else
            {
                firstPoint = new Point(length, Distance.Zero);
                firstPoint = firstPoint.Rotate2D(startingAngle);
            }
            List<Point> points = new List<Point>() { firstPoint };
            for (int i = 1; i < numberOfSides; i++)
            {
                points.Add(firstPoint.Rotate2D(step * i));
            }
            if (centerPoint == null)
            {
                return new Polygon(points, false);
            }
            else
            {
                return new Polygon(points.Select(p => p + centerPoint).ToList(), false);
            }
        }

        /// <summary>
        /// Creates a polygon with the sidelength and this angle at the origin
        /// </summary>
        public static Polygon Rhombus(Angle angle, Distance sideLength)
        {
            var vector1 = Direction.Right * sideLength;
            var vector2 = vector1.Rotate(new Rotation(angle));
            return Parallelogram(vector1, vector2);
        }

        public static Polygon Square(Distance sideLength, Point basePoint = null)
        {
            return Rectangle(sideLength, sideLength, basePoint);
        }

        public static Polygon Triangle(Vector vector1, Vector vector2, Point basePoint = null)
        {
            if (basePoint == null)
            {
                basePoint = vector1.BasePoint;
            }
            else
            {
                vector1 = new Vector(basePoint, vector1);
            }
            vector2 = new Vector(basePoint, vector2);

            var point1 = basePoint;
            var point2 = vector1.EndPoint;
            var point3 = vector2.EndPoint;

            return new Polygon(new List<Point>() { point1, point2, point3 });
        }
    }
}
