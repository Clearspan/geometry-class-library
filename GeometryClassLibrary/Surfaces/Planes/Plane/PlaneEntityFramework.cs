﻿namespace GeometryClassLibrary
{
    public partial class Plane
    {
        public int? DatabaseId { get; set; }

        public int? BasePoint_DatabaseId { get; set; }

        public int? NormalVector_DatabaseId { get; set; }
    }
}
