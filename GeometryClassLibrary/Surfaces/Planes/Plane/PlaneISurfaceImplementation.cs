﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeometryClassLibrary
{
    public partial class Plane : ISurface
    {
        public virtual bool IsBounded { get { return false; } }

        ISurface ISurface.Shift(Shift shift)
        {
            return this.Shift(shift);
        }
    }
}
