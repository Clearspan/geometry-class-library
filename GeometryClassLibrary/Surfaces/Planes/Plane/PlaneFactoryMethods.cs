﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeometryClassLibrary
{
    public partial class Plane
    {
        public readonly static Plane XY = new Plane(Line.ZAxis);
        public readonly static Plane XZ = new Plane(Line.YAxis);
        public readonly static Plane YZ = new Plane(Line.XAxis);
    }
}
