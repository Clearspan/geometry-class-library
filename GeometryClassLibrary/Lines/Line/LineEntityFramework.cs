﻿namespace GeometryClassLibrary
{
    public partial class Line
    {
        public int? DatabaseId { get; set; }

        public int? BasePoint_DatabaseId { get; set; }

        public int? Direction_DatabaseId { get; set; }
    }
}
