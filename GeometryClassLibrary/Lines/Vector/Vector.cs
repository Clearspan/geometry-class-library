﻿using System;
using Newtonsoft.Json;
using UnitClassLibrary;
using static UnitClassLibrary.Distance;

namespace GeometryClassLibrary
{
    /// <summary>
    /// A vector is a line segment that has a direction
    /// Except it derives from Line. So that it doesn't cut the way linesegments do.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public partial class Vector : Line
    {
        #region Properties and Fields

        public static readonly Vector Zero = new Vector(Point.Origin);

        /// <summary>
        /// Returns the magnitude of the vector
        /// </summary>
        [JsonProperty]
        public virtual Distance Magnitude
        {
            get { return _magnitude; }
            set { _magnitude = value; }
        }
        private Distance _magnitude;

        /// <summary>
        /// Returns the x-component of this vector
        /// </summary>
        public virtual Distance XComponent
        {
            get { return _magnitude * base.Direction.XComponent; }
        }

        /// <summary>
        /// Returns the y-component of this vector
        /// </summary>
        public virtual Distance YComponent
        {
            get { return _magnitude * base.Direction.YComponent; }
        }

        /// <summary>
        /// Returns the z-component of this vector
        /// </summary>
        public virtual Distance ZComponent
        {
            get { return _magnitude * base.Direction.ZComponent; }
        }

        /// <summary>
        /// Returns the point that is the distance away from the Vector's current basepoint that is equal to the vector's magnitude in the vector's direction
        /// </summary>
        public virtual Point EndPoint
        {
            get { return new Point(XComponent, YComponent, ZComponent) + BasePoint; }
        }

        ///// <summary>
        ///// Allows the xyz components of the vector to be able to be accessed as an array
        ///// </summary>
        ///// <param name="i"></param>
        ///// <returns></returns>
        //private Distance this[int i]
        //{
        //    get
        //    {
        //        if (i == 0)
        //            return XComponent;
        //        else if (i == 1)
        //            return YComponent;
        //        else if (i == 2)
        //            return ZComponent;
        //        else
        //            throw new Exception("No item of that index!");
        //    }
        //}

        #endregion

        #region Constructors

        /// <summary>
        /// Null Constructor
        /// </summary>   
        protected Vector() { }

        /// <summary>
        /// Creates a vector that extends from the Origin to the passed reference point  
        /// </summary>
        /// <param name="passedEndPoint">The point at which the vector goes to / ends at</param>
        public Vector(Point passedEndPoint)
            : base(passedEndPoint)
        {
            _magnitude = passedEndPoint.DistanceTo(Point.Origin);
        }

        /// <summary>
        /// Creates a vector that starts at the given base point and goes to the given end point
        /// </summary>
        public Vector(Point basePoint, Point endPoint)
            : base(basePoint, endPoint)
        {
            _magnitude = basePoint.DistanceTo(endPoint);
        }

        /// <summary>
        /// Creates a new vector with the given BasePoint in the same direction and magnitude as the passed Vector
        /// </summary>
        public Vector(Point basePoint, Vector vector)
            : base(vector.Direction, basePoint)
        {
            _magnitude = new Distance(vector._magnitude);
        }

        public Vector(Direction direction, Distance magnitude) : base(direction)
        {
            this._magnitude = magnitude;
        }

        public Vector(Line line, Distance magnitude) : this(line.BasePoint, line.Direction, magnitude) { }

        public Vector(Vector vector, Distance magnitude) : this(vector.BasePoint, vector.Direction, magnitude) { }

        /// <summary>
        /// Creates a new vector with the given BasePoint in the given direction with the given magnitude
        /// </summary>
        public Vector(Point passedBasePoint, Direction direction, Distance magnitude = null)
            : base(direction, passedBasePoint)
        {
            if (magnitude == null)
            {
                _magnitude = Inch;
            }
            else
            {
                _magnitude = magnitude;
            }
        }

        /// <summary>
        /// Creates a Vector with the same basepoint and end point as this edge
        /// </summary>
        public Vector(IEdge edge) : this(edge.BasePoint, edge.EndPoint) { }

        /// <summary>
        /// Default copy constructor
        /// </summary>
        public Vector(Vector toCopy)
            : this(toCopy.BasePoint, toCopy.Direction, toCopy.Magnitude) { }

        #endregion

        #region Overloaded Operators

        public override int GetHashCode()
        {
            return this.BasePoint.GetHashCode() ^ this.EndPoint.GetHashCode();
        }

        /// <summary>
        /// Adds the two vectors and returns the resultant vector
        /// </summary>
        /// <param name="passedVector1"></param>
        /// <param name="passedVector2"></param>
        /// <returns></returns>
        public static Vector operator +(Vector passedVector1, Vector passedVector2)
        {
            //Recreates Vector 2 with its base point at the end point of Vector 1
            Vector relocatedVector2 = new Vector(passedVector1.EndPoint, passedVector2);

            Point newBasePoint = passedVector1.BasePoint; //The new vector has the same base point as Vector 1
            Point newEndPoint = relocatedVector2.EndPoint; //The new vector has the same end point as the relocated Vector 2

            return new Vector(newBasePoint, newEndPoint);
        }

        /// <summary>
        /// Subtracts the two vectors and returns the resultant vector
        /// </summary>
        /// <param name="passedVector1"></param>
        /// <param name="passedVector2"></param>
        /// <returns></returns>
        public static Vector operator -(Vector passedVector1, Vector passedVector2)
        {
            Vector negatedVector2 = -1 * passedVector2;
            return passedVector1 + negatedVector2;
        }

        /// <summary>
        /// Returns a new Vector with each component multiplied by the scalar (order of terms does not matter)
        /// </summary>
        /// <param name="passedVector"></param>
        /// <param name="scalar"></param>
        /// <returns></returns>
        public static Vector operator *(Vector passedVector, double scalar)
        {
            return new Vector(passedVector.BasePoint, passedVector.Direction, passedVector.Magnitude * scalar);
        }

        /// <summary>
        /// Returns a new Vector with each component multiplied by the scalar (order of terms does not matter)
        /// </summary>
        /// <param name="scalar"></param>
        /// <param name="passedVector"></param>
        /// <returns></returns>
        public static Vector operator *(double scalar, Vector passedVector)
        {
            return passedVector * scalar;
        }

        /// <summary>
        /// Returns a new Vector with each component divided by the divisor
        /// </summary>
        /// <param name="passedVector1"></param>
        /// <param name="divisor"></param>
        /// <returns></returns>
        public static Vector operator /(Vector passedVector, double divisor)
        {
            return new Vector(passedVector.BasePoint, passedVector.Direction, passedVector.Magnitude / divisor);
        }

        public static bool operator ==(Vector vector1, Vector vector2)
        {
            if ((object)vector1 == null)
            {
                if ((object)vector2 == null)
                {
                    return true;
                }
                return false;
            }
            return vector1.Equals(vector2);
        }

        public static bool operator !=(Vector vector1, Vector vector2)
        {
            if ((object)vector1 == null)
            {
                if ((object)vector2 == null)
                {
                    return false;
                }
                return true;
            }
            return !vector1.Equals(vector2);
        }

        public override bool Equals(object obj)
        {
            //check for null
            if (obj == null || !(obj is Vector))
            {
                return false;
            }
            
            Vector vec = (Vector)obj;

            bool basePointsEqual = (this.BasePoint == vec.BasePoint);
            bool endPointsEqual = (this.EndPoint == vec.EndPoint);

            return basePointsEqual && endPointsEqual;        
        }

        ///// <summary>
        ///// returns the comparison integer of -1 if less than, 0 if equal to, and 1 if greater than the other segment
        ///// NOTE: BASED SOLELY ON LENGTH.  MAY WANT TO CHANGE LATER
        ///// </summary>
        ///// <param name="other"></param>
        ///// <returns></returns>
        //public int CompareTo(Vector other)
        //{
        //    if (this._magnitude.Equals(other._magnitude))
        //        return 0;
        //    else
        //        return this._magnitude.CompareTo(other._magnitude);
        //}

        /// <summary>
        /// returns the vector as a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("BasePoint= {0}, Direction= {1}, Magnitude= {2}", this.BasePoint.ToString(), this.Direction.ToString(), this.Magnitude.ToString());
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new vector with the same base point and direction but a different magnitude.
        /// </summary>
        public Vector Resize(Distance newMagnitude)
        {
            return new Vector(BasePoint, Direction, newMagnitude);
        }

       
        //// Do we need this method?
        ///// <summary>
        ///// Determines whether or not the two Vectors in the same direction overlap at all partially or completely 
        ///// </summary>
        ///// <param name="potentiallyOverlappingVector">The vector to see if we overlap with</param>
        ///// <returns>Returns true if the vectors overlap at all or one contains the other</returns>
        //public bool DoesOverlapInSameDirection(Vector potentiallyOverlappingVector)
        //{
        //    //see if we partially overlap
        //    bool doesSharePoint = this.Contains(potentiallyOverlappingVector.EndPoint) || this.Contains(potentiallyOverlappingVector.BasePoint);
        //    bool partiallyOverlap = this.HasSameDirectionAs(potentiallyOverlappingVector) && doesSharePoint;

        //    //or completely contain the other
        //    bool doesContainOneOrOther = this.Contains(potentiallyOverlappingVector) || potentiallyOverlappingVector.Contains(this);

        //    return partiallyOverlap || doesContainOneOrOther;
        //}

        /// <summary>
        /// Projects this LineSegment onto the given Line, which is the projected length of this LineSegment in the direction of the Line projected onto
        /// </summary>
        /// <param name="projectOnto">the Line on which to project the LineSegment</param>
        /// <returns></returns>
        public Vector ProjectOntoLine(Line projectOnto)
        {
            if (this.IsCoplanarWith(projectOnto))
            {
                Point basePoint = this.BasePoint.ProjectOntoLine(projectOnto);
                Point endPoint = this.EndPoint.ProjectOntoLine(projectOnto);
                return new Vector(basePoint, endPoint);
            }
            throw new Exception("Cannot project a vector onto a line that its not coplanar with!");
        }

        /// <summary>
        /// Projects this vector on the given plane
        /// </summary>
        /// <param name="projectOnto">The Plane to project this Vector onto</param>
        /// <returns>Returns a new Vector that is this Vector projected onto the Plane</returns>
        public new Vector ProjectOntoPlane(Plane plane)
        {
            //find the line in the plane and then project this line onto it
            Point newBasePoint = BasePoint.ProjectOntoPlane(plane);
            Point newEndPoint = EndPoint.ProjectOntoPlane(plane);
            return new Vector(newBasePoint, newEndPoint);
        }

        /// <summary>
        /// Returns the cross product of the 2 vectors
        /// which is always perpendicular to both vectors
        /// and whose magnitude is the area of the parellelogram spanned by those two vectors
        /// Consequently if they point in the same (or opposite) direction, than the cross product is zero
        /// </summary>
        public Vector CrossProduct(Vector passedVector)
        {
            Vector v1 = new Vector(this);
            Vector v2 = new Vector(passedVector);

            //Find each component 

            double xProduct1 = v1.YComponent.Inches * v2.ZComponent.Inches;
            double xProduct2 = v1.ZComponent.Inches * v2.YComponent.Inches;

            double yProduct1 = v1.ZComponent.Inches * v2.XComponent.Inches;
            double yProduct2 = v1.XComponent.Inches * v2.ZComponent.Inches;

            double zProduct1 = v1.XComponent.Inches * v2.YComponent.Inches;
            double zProduct2 = v1.YComponent.Inches * v2.XComponent.Inches;

            double newX = (xProduct1) - (xProduct2);
            double newY = (yProduct1) - (yProduct2);
            double newZ = (zProduct1) - (zProduct2);

            return new Vector(Point.MakePointWithInches(newX, newY, newZ));
        }

        /// <summary>
        /// determines whether two vectors point in the same direction
        /// </summary>
        public bool HasSameDirectionAs(Vector passedVector)
        {
            Vector vector1 = this;
            Vector vector2 = passedVector;

            if (vector2 == null)
            {
                return false; //Doesn't have a direction. Should be false
            }
            if (vector1.Magnitude == Distance.Zero || vector2.Magnitude == Distance.Zero)
            {
                return true;
            }
            return vector1.Direction == vector2.Direction;
            
            
        }

        /// <summary>
        /// determines whether two vectors point in opposite directions 
        /// </summary>
        /// <param name="v1">vector to compare against</param>
        /// <returns></returns>
        public bool HasOppositeDirectionOf(Vector passedVector)
        {
            Vector vector1 = this;
            Vector vector2 = passedVector;
            
            //return (vector1.Direction == vector2.Direction.Reverse());
            return vector1.AngleBetween(vector2) == new Angle(AngleType.Degree, 180);
        }

        /// <summary>
        /// determines whether two vectors point in the same or opposite directions.
        /// </summary>
        public bool HasSameOrOppositeDirectionAs(Vector v1)
        {
            bool sameDirection = this.HasSameDirectionAs(v1);
            bool oppositeDirections = this.HasOppositeDirectionOf(v1);

            return sameDirection || oppositeDirections;
        }

        /// <summary>
        /// returns a vector with its base and end points swapped.
        /// </summary>
        public Vector Reverse()
        {
            return new Vector(this.EndPoint, this.BasePoint);
        }
        
       /// <summary>
        /// Flips the vector about its tail.
        /// Like this:  <-------|------->
       /// </summary>
       /// <returns></returns>
        public Vector FlipAboutTail()
        {
            Vector reversed = this.Reverse();
            return new Vector(this.BasePoint, (reversed + reversed).EndPoint);
        }

        /// <summary>
        /// Flips the vector about its head.
        /// Like this: ------>|<-------
        /// </summary><
        public Vector FlipAboutHead()
        {
            Vector result = this.Shift(this).Reverse();
            return result;
        }

        /// <summary>
        /// Converts a vector into a matrix with one column
        /// </summary>
        /// <returns></returns>
        public Matrix ConvertToMatrixColumn()
        {
            Matrix returnMatrix = new Matrix(3, 1);
            double[] vectorArray = { XComponent.Inches, YComponent.Inches, ZComponent.Inches };
            returnMatrix.SetColumn(0, vectorArray);
            return returnMatrix;
        }

        /// <summary>
        /// Rotates the vector about the given axis by the passed angle
        /// </summary>
        public new Vector Rotate(Rotation rotationToApply)
        {
            Point rotatedBasePoint = this.BasePoint.Rotate3D(rotationToApply);
            Point rotatedEndPoint = this.EndPoint.Rotate3D(rotationToApply);
            return new Vector(rotatedBasePoint, rotatedEndPoint);
        }

        /// <summary>
        /// Performs the Shift on this vector
        /// </summary>
        /// <param name="passedShift"></param>
        /// <returns></returns>
        public new Vector Shift(Shift passedShift)
        {
            return new Vector(BasePoint.Shift(passedShift), EndPoint.Shift(passedShift));
        }

        /// <summary>
        /// Translates the vector the given distance in the given direction
        /// </summary>
        /// <param name="passedDirection"></param>
        /// <param name="passedDisplacement"></param>
        /// <returns></returns>
        public new Vector Translate(Translation translation)
        {
            Point newBasePoint = this.BasePoint.Translate(translation);
            Point newEndPoint = this.EndPoint.Translate(translation);

            return new Vector(newBasePoint, newEndPoint);
        }

        /// <summary>
        /// Returns a unit vector with a length of 1 in with the given Distance that is equivalent to this direction
        /// Note: if it is a zero vector and you call the unitVector it will throw an exception.
        /// </summary>
        public new Vector UnitVector(DistanceType passedType)
        {
            if (Magnitude == Distance.Zero)
            {
                // return new Vector(Point.Origin);
                throw new Exception();
            }
            else return Direction.UnitVector(passedType);
        }
        /// <summary>
        /// Returns the DotProduct between two Vectors as a distance
        /// </summary>
        public Area DotProduct(Vector vector)
        {
            Vector vector1 = this;
            Vector vector2 = vector;

            var xTerm = vector1.XComponent * vector2.XComponent;
            var yTerm = vector1.YComponent * vector2.YComponent;
            var zTerm = vector1.ZComponent * vector2.ZComponent;
            
            var sum = xTerm + yTerm + zTerm;
            return sum;
        }
       
        public bool IsPerpendicularTo(Vector other)
        {
            return this.SmallestAngleBetween(other) == 90 * Angle.Degree;
        }

        public bool IsParallelTo(Vector vector)
        {
            return this.SmallestAngleBetween(vector) == Angle.Zero;
        }

     

       


        #endregion

    }


}
